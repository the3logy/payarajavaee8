package com.acme.customers.lib.v1;

public enum CustomerStatus {
    Merchant, Customer, Company
}
