package com.acme.customers.lib.v1.common;

import lombok.Data;

import java.util.Date;

@Data
public class BaseType {
    private String id;
    private Date updateAt;
    private Date createdAt;
}
