package com.acme.customers.api.rest.v1.resources;

import com.acme.customers.lib.v1.Customer;
import com.acme.customers.lib.v1.CustomerStatus;
import com.acme.customers.lib.v1.response.CustomerList;
import org.apache.derby.client.am.SqlException;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.sql.DataSource;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("/customers")
@ApplicationScoped
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CustomerResource {

    @Resource(lookup = "java:global/CustomerDS")
    private DataSource dataSource;

    @PostConstruct
    private void init() {
        Connection con = null;

        try {
            con = dataSource.getConnection();
            PreparedStatement stmt = con.prepareStatement(
              "CREATE TABLE customers(" +
                      "id varchar(36) primary key, first_name varchar(255), last_name varchar(255)," +
                      "email varchar(255), status varchar(255), date_of_birth TIMESTAMP, " +
                      "update_at TIMESTAMP, created_at TIMESTAMP)"
            );
            stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getAnonymousLogger().log(Level.WARNING, ex.getMessage());
        } finally {
            try {
                if(con != null) con.close();
            } catch(SQLException ignored){}
        }
    }

    @GET
    public Response getCustomers(@QueryParam("limit") Integer limit, @QueryParam("offset") Integer offset)
            throws SQLException {
        List<Customer> customers = new ArrayList<>();
        String query = "SELECT * FROM customers ORDER By id";

        if(limit != null && limit > 0){
            query += " LIMIT " + limit;
        }

        if(offset != null && offset > 0){
            query += " OFFSET " + offset;
        }

        Connection connection = dataSource.getConnection();
        PreparedStatement stmt = connection.prepareStatement(query);
        ResultSet resultSet = stmt.executeQuery();

        while(resultSet.next()){
            Customer customer = new Customer();
            customer.setId(resultSet.getString("id"));
            customer.setUpdateAt(resultSet.getDate("updated_at"));
            customer.setCreatedAt(resultSet.getDate("created_at"));
            customer.setFirstName(resultSet.getString("first_name"));
            customer.setLastName(resultSet.getString("last_name"));
            customer.setEmail(resultSet.getString("email"));
            customer.setDateOfBirth(resultSet.getTimestamp("date_of_birth"));
            customer.setStatus(CustomerStatus.valueOf(resultSet.getString("status")));

            customers.add(customer);
        }

        connection.close();

        CustomerList customerList = new CustomerList();
        customerList.setCustomers(customers);

        return Response.ok(customerList).header("X-Total-Count", 0).build();
    }

    @POST
    public Response createCustomer(Customer ncustomer) throws SQLException {

        Connection connection = dataSource.getConnection();
        PreparedStatement stmt = connection.prepareStatement("INSERT INTO customers " +
                "(id, first_name, last_name, email, date_of_birth, status, created_at, updated_at) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
        String id = UUID.randomUUID().toString();

        stmt.setString(1, id);
        stmt.setString(2, ncustomer.getFirstName());
        stmt.setString(3, ncustomer.getLastName());
        stmt.setString(4, ncustomer.getEmail());

        if(ncustomer.getDateOfBirth() != null ){
            stmt.setTimestamp(5, new Timestamp(ncustomer.getDateOfBirth().getTime()));
        }

        if(ncustomer.getStatus() != null){
            stmt.setString(6, ncustomer.getStatus().toString());
        }

        Timestamp timestampNow = new Timestamp(java.util.Date.from(Instant.now()).getTime());
        stmt.setTimestamp(7, timestampNow);
        stmt.setTimestamp(8, timestampNow);

        stmt.executeUpdate();

        ncustomer.setId(id);
        ncustomer.setCreatedAt(timestampNow);
        ncustomer.setUpdateAt(timestampNow);
        connection.close();

        return Response.ok(ncustomer).build();
    }

    @GET
    @Path("/{id}")
    public Response getCustomer(@PathParam("id") String id) throws SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement stmt = connection.prepareStatement("SELECT * FROM customers WHERE id = ?");
        stmt.setString(1, id);
        ResultSet resultSet = stmt.executeQuery();

        while(resultSet.next()){
            Customer customer = new Customer();
            customer.setId(resultSet.getString("id"));
            customer.setUpdateAt(resultSet.getDate("updated_at"));
            customer.setCreatedAt(resultSet.getDate("created_at"));
            customer.setFirstName(resultSet.getString("first_name"));
            customer.setLastName(resultSet.getString("last_name"));
            customer.setEmail(resultSet.getString("email"));
            customer.setDateOfBirth(resultSet.getTimestamp("date_of_birth"));
            customer.setStatus(CustomerStatus.valueOf(resultSet.getString("status")));

            connection.close();
            return Response.ok(customer).build();
        }

        return Response.noContent().build();
    }
}
